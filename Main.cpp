#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit {
	CLUBS,
	HEARTS,
	SPADES,
	DIAMONDS
};

enum Rank {
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};


struct Card {
	Suit suit;
	Rank rank;
};

void PrintCard(Card card) {
	string rankName, suitName;

	switch (card.rank) {

	case(0): {
		rankName = "Two";
		break;
	}
	case(1): {
		rankName = "Three";
		break;
	}
	case(2): {
		rankName = "Four";
		break;
	}
	case(3): {
		rankName = "Five";
		break;
	}
	case(4): {
		rankName = "Six";
		break;
	}
	case(5): {
		rankName = "Seven";
		break;
	}
	case(6): {
		rankName = "Eight";
		break;
	}
	case(7): {
		rankName = "Nine";
		break;
	}
	case(8): {
		rankName = "Ten";
		break;
	}
	case(9): {
		rankName = "Jack";
		break;
	}
	case(10): {
		rankName = "Queen";
		break;
	}
	case(11): {
		rankName = "King";
		break;
	}
	case(12): {
		rankName = "Ace";
		break;
	}

	}

	switch (card.suit) {

	case(0): {
		suitName = "Clubs";
		break;
	}
	case(1): {
		suitName = "Hearts";
		break;
	}
	case(2): {
		suitName = "Spades";
		break;
	}
	case(3): {
		suitName = "Diamonds";
		break;
	}
	}

	cout << "The " << rankName << " of " << suitName << "\n";
}

Card HighCard(Card a, Card b) {
	if (a.rank > b.rank) {
		return a;
	}
	return b;
}

int main() {

	Card first;
	first.rank = ACE;
	first.suit = DIAMONDS;

	Card second;
	second.rank = KING;
	second.suit = SPADES;

	PrintCard(first);
	PrintCard(second);

	PrintCard(HighCard(first, second));

	_getch();
	return 1;
}